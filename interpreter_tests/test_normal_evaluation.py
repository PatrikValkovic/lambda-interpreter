import copy

from interpreter_tests.colors import GREEN, END
from interpreter.variable import Variable
from interpreter.expression import Expression
from interpreter.lambda_element import Lambda


def test_beta_reduction():
    print('Test beta reduction: ', end='')

    expression_0 = Expression([
        Lambda(['x'], Expression([
            Variable('x')
        ])),
        Variable(8)
    ])

    eval_expression_0 = copy.deepcopy(expression_0)
    eval_expression_0.normal_evaluation()
    assert eval_expression_0.representation() == '8'

    assert expression_0.representation() == '((lambda x. x) 8)'
    assert expression_0.beta_reduction()
    assert expression_0.representation() == '8'
    assert not expression_0.beta_reduction()

    expression_1 = Expression([
        Lambda(['x'], Expression([
            Variable('x'),
            Variable('x')
        ])),
        Variable(8)
    ])

    eval_expression_1 = copy.deepcopy(expression_1)
    eval_expression_1.normal_evaluation()
    assert eval_expression_1.representation() == '(8 8)'

    assert expression_1.representation() == '((lambda x. (x x)) 8)'
    assert expression_1.beta_reduction()
    assert expression_1.representation() == '(8 8)'
    assert not expression_1.beta_reduction()

    expression_2 = Expression([
        Lambda(['x','y'], Expression([
            Variable('y'),
            Variable('x')
        ])),
        Variable(7),
        Variable(8)
    ])

    eval_expression_2 = copy.deepcopy(expression_2)
    eval_expression_2.normal_evaluation()
    assert eval_expression_2.representation() == '(8 7)'

    assert expression_2.representation() == '((lambda x y. (y x)) 7 8)'
    assert expression_2.beta_reduction()
    assert expression_2.representation() == '((lambda y. (y 7)) 8)'
    assert expression_2.beta_reduction()
    assert expression_2.representation() == '(8 7)'
    assert not expression_2.beta_reduction()

    expression_3 = Expression([
        Lambda(['x'], Expression([
            Variable('x')
        ])),
        Lambda(['y'], Expression([
            Variable('y')
        ])),
    ])

    eval_expression_3 = copy.deepcopy(expression_3)
    eval_expression_3.normal_evaluation()
    assert eval_expression_3.representation() == '(lambda y. y)'

    assert expression_3.representation() == '((lambda x. x) (lambda y. y))'
    assert expression_3.beta_reduction()
    assert expression_3.representation() == '(lambda y. y)'
    assert not expression_3.beta_reduction()

    expression_4 = Expression([
        Lambda(['x'], Expression([
            Variable('x')
        ])),
        Lambda(['y'], Expression([
            Variable('y')
        ])),
        Lambda(['z'], Expression([
            Variable('z')
        ])),
    ])

    eval_expression_4 = copy.deepcopy(expression_4)
    eval_expression_4.normal_evaluation()
    assert eval_expression_4.representation() == '(lambda z. z)'

    assert expression_4.representation() == '((lambda x. x) (lambda y. y) (lambda z. z))'
    assert expression_4.beta_reduction()
    assert expression_4.representation() == '((lambda y. y) (lambda z. z))'
    assert expression_4.beta_reduction()
    assert expression_4.representation() == '(lambda z. z)'

    expression_5 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Variable('y'),
                Variable('y')
            ])),
            Variable(8),
            Variable('x')
        ])),
        Variable(3)
    ])

    eval_expression_5 = copy.deepcopy(expression_5)
    eval_expression_5.normal_evaluation()
    assert eval_expression_5.representation() == '((8 8) 3)'

    assert expression_5.representation() == '((lambda x. ((lambda y. (y y)) 8 x)) 3)'
    assert expression_5.beta_reduction()
    assert expression_5.representation() == '((lambda y. (y y)) 8 3)'
    assert expression_5.beta_reduction()
    assert expression_5.representation() == '((8 8) 3)'
    assert not expression_5.beta_reduction()

    expression_6 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Variable('x'), Variable('y')
            ])),
            Variable('x')
        ])),
        Lambda(['z'], Expression([
            Variable('z')
        ]))
    ])

    eval_expression_6 = copy.deepcopy(expression_6)
    eval_expression_6.normal_evaluation()
    assert eval_expression_6.representation() == '(lambda z. z)'

    assert expression_6.representation() == '((lambda x. ((lambda y. (x y)) x)) (lambda z. z))'
    assert expression_6.beta_reduction()
    assert expression_6.representation() == '((lambda y. ((lambda z. z) y)) (lambda z. z))'
    assert expression_6.beta_reduction()
    assert expression_6.representation() == '((lambda z. z) (lambda z. z))'
    assert expression_6.beta_reduction()
    assert expression_6.representation() == '(lambda z. z)'

    expression_7 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Lambda(['z'], Expression([
                    Variable('x'), Variable('y')
                ])),
                Variable('z')
            ])),
            Variable('f')
        ]))
    ])

    eval_expression_7 = copy.deepcopy(expression_7)
    eval_expression_7.normal_evaluation()
    assert eval_expression_7.representation() == '(lambda x. (x f))'

    assert expression_7.representation() == '(lambda x. ((lambda y. ((lambda z. (x y)) z)) f))'
    assert expression_7.beta_reduction()
    assert expression_7.representation() == '(lambda x. ((lambda z. (x f)) z))'
    assert expression_7.beta_reduction()
    assert expression_7.representation() == '(lambda x. (x f))'
    assert not expression_7.beta_reduction()

    expression_8 = Expression([
        Lambda(['x'], Expression([
            Variable('x'),
            Variable('x')
        ])),
        Lambda(['z'], Variable('z'))
    ])

    eval_expression_8 = copy.deepcopy(expression_8)
    eval_expression_8.normal_evaluation()
    assert eval_expression_8.representation() == '(lambda z. z)'

    assert expression_8.representation() == '((lambda x. (x x)) (lambda z. z))'
    assert expression_8.beta_reduction()
    assert expression_8.representation() == '((lambda z. z) (lambda z. z))'
    assert expression_8.beta_reduction()
    assert expression_8.representation() == '(lambda z. z)'

    expression_9 = Expression([
        Expression([
            Lambda(['x'], Variable('x')),
            Lambda(['y'], Variable('y'))
        ]),
        Lambda(['z'], Variable('z')),
        Lambda(['x'], Variable('x'))
    ])

    eval_expression_9 = copy.deepcopy(expression_9)
    eval_expression_9.normal_evaluation()
    assert eval_expression_9.representation() == '(lambda x. x)'

    assert expression_9.representation() == '(((lambda x. x) (lambda y. y)) (lambda z. z) (lambda x. x))'
    assert expression_9.beta_reduction()
    assert expression_9.representation() == '((lambda y. y) (lambda z. z) (lambda x. x))'
    assert expression_9.beta_reduction()
    assert expression_9.representation() == '((lambda z. z) (lambda x. x))'
    assert expression_9.beta_reduction()
    assert expression_9.representation() == '(lambda x. x)'
    assert not expression_9.beta_reduction()

    expression_10 = Expression([
        Expression([
            Lambda(['c', 'f'], Expression([
                Lambda(['c', 'y'], Expression([
                    Variable('c'), Variable('y')
                ])),
                Variable('f')
            ])),
            Variable(1), Variable(2)
        ]),
        Expression([
            Lambda(['x'], Variable('x')),
            Variable('3')
        ])
    ])

    eval_expression_10 = copy.deepcopy(expression_10)
    eval_expression_10.normal_evaluation()
    assert eval_expression_10.representation() == '(2 3)'

    assert expression_10.representation() == '(((lambda c f. ((lambda c y. (c y)) f)) 1 2) ((lambda x. x) 3))'
    assert expression_10.beta_reduction()
    assert expression_10.representation() == '(((lambda f. ((lambda c y. (c y)) f)) 2) ((lambda x. x) 3))'
    assert expression_10.beta_reduction()
    assert expression_10.representation() == '(((lambda c y. (c y)) 2) ((lambda x. x) 3))'
    assert expression_10.beta_reduction()
    assert expression_10.representation() == '((lambda y. (2 y)) ((lambda x. x) 3))'
    assert expression_10.beta_reduction()
    assert expression_10.representation() == '(2 ((lambda x. x) 3))'
    assert expression_10.beta_reduction()
    assert expression_10.representation() == '(2 3)'
    assert not expression_10.beta_reduction()

    print(GREEN + 'Passed' + END)
