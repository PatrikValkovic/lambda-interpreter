import copy

from interpreter.expression import Expression
from interpreter.lambda_element import Lambda
from interpreter.variable import Variable
from interpreter_tests.colors import GREEN, END


def test_applicative_evaluation():
    print('Test applicative evaluation: ', end='')

    expression_0 = Expression([
        Lambda(['x', 'y'], Expression([
            Variable('y'),
            Variable('x')
        ])),
        Variable(7),
        Variable(8)
    ])

    eval_expression_0 = copy.deepcopy(expression_0)
    eval_expression_0.applicative_evaluation()
    assert eval_expression_0.representation() == '(8 7)'

    assert expression_0.representation() == '((lambda x y. (y x)) 7 8)'
    assert expression_0.applicative_beta_reduction()
    assert expression_0.representation() == '((lambda y. (y 7)) 8)'
    assert expression_0.applicative_beta_reduction()
    assert expression_0.representation() == '(8 7)'
    assert not expression_0.applicative_beta_reduction()

    expression_1 = Expression([
        Lambda(['x'], Expression([
            Variable('x')
        ])),
        Lambda(['y'], Expression([
            Variable('y')
        ])),
        Lambda(['z'], Expression([
            Variable('z')
        ])),
    ])

    eval_expression_1 = copy.deepcopy(expression_1)
    eval_expression_1.applicative_evaluation()
    assert eval_expression_1.representation() == '(lambda z. z)'

    assert expression_1.representation() == '((lambda x. x) (lambda y. y) (lambda z. z))'
    assert expression_1.applicative_beta_reduction()
    assert expression_1.representation() == '((lambda y. y) (lambda z. z))'
    assert expression_1.applicative_beta_reduction()
    assert expression_1.representation() == '(lambda z. z)'
    assert not expression_1.applicative_beta_reduction()

    expression_2 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Variable('y')),
            Variable(2),
            Variable('x')
        ]))
    ])

    eval_expression_2 = copy.deepcopy(expression_2)
    eval_expression_2.applicative_evaluation()
    assert eval_expression_2.representation() == '(lambda x. (2 x))'

    assert expression_2.representation() == '(lambda x. ((lambda y. y) 2 x))'
    assert expression_2.applicative_beta_reduction()
    assert expression_2.representation() == '(lambda x. (2 x))'
    assert not expression_2.applicative_beta_reduction()

    expression_3 = Expression([
        Variable('x'),
        Variable('y'),
        Lambda(['x'], Expression([
            Lambda(['y'], Variable('x')),
            Variable(3)
        ]))
    ])

    eval_expression_3 = copy.deepcopy(expression_3)
    eval_expression_3.applicative_evaluation()
    assert eval_expression_3.representation() == '(x y (lambda x. x))'

    assert expression_3.representation() == '(x y (lambda x. ((lambda y. x) 3)))'
    assert expression_3.applicative_beta_reduction()
    assert expression_3.representation() == '(x y (lambda x. x))'
    assert not expression_3.applicative_beta_reduction()

    expression_4 = Expression([
        Lambda(['x', 'y'], Expression([
            Lambda(['z'], Expression([
                Lambda(['f'], Expression([
                    Variable('z'),
                    Variable('f')
                ])),
                Variable('z')
            ])),
            Variable('x')
        ])),
        Variable(3),
        Variable(7)
    ])

    eval_expression_4 = copy.deepcopy(expression_4)
    eval_expression_4.applicative_evaluation()
    assert eval_expression_4.representation() == '(3 3)'

    assert expression_4.representation() == '((lambda x y. ((lambda z. ((lambda f. (z f)) z)) x)) 3 7)'
    assert expression_4.applicative_beta_reduction()
    assert expression_4.representation() == '((lambda x y. ((lambda z. (z z)) x)) 3 7)'
    assert expression_4.applicative_beta_reduction()
    assert expression_4.representation() == '((lambda x y. (x x)) 3 7)'
    assert expression_4.applicative_beta_reduction()
    assert expression_4.representation() == '((lambda y. (3 3)) 7)'
    assert expression_4.applicative_beta_reduction()
    assert expression_4.representation() == '(3 3)'
    assert not expression_4.applicative_beta_reduction()

    expression_5 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Lambda(['z'], Variable('z')),
                Variable(1)
            ])),
            Variable(2)
        ])),
        Variable(3)
    ])

    eval_expression_5 = copy.deepcopy(expression_5)
    eval_expression_5.applicative_evaluation()
    assert eval_expression_5.representation() == '1'

    assert expression_5.representation() == '((lambda x. ((lambda y. ((lambda z. z) 1)) 2)) 3)'
    assert expression_5.applicative_beta_reduction()
    assert expression_5.representation() == '((lambda x. ((lambda y. 1) 2)) 3)'
    assert expression_5.applicative_beta_reduction()
    assert expression_5.representation() == '((lambda x. 1) 3)'
    assert expression_5.applicative_beta_reduction()
    assert expression_5.representation() == '1'
    assert not expression_5.applicative_beta_reduction()

    expression_6 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Variable('y')),
            Lambda(['z'], Variable('z')),
            Variable('x')
        ])),
        Lambda(['a'], Variable('a'))
    ])

    eval_expression_6 = copy.deepcopy(expression_6)
    eval_expression_6.applicative_evaluation()
    assert eval_expression_6.representation() == '(lambda a. a)'

    assert expression_6.representation() == '((lambda x. ((lambda y. y) (lambda z. z) x)) (lambda a. a))'
    assert expression_6.applicative_beta_reduction()
    assert expression_6.representation() == '((lambda x. ((lambda z. z) x)) (lambda a. a))'
    assert expression_6.applicative_beta_reduction()
    assert expression_6.representation() == '((lambda x. x) (lambda a. a))'
    assert expression_6.applicative_beta_reduction()
    assert expression_6.representation() == '(lambda a. a)'
    assert not expression_6.applicative_beta_reduction()

    expression_7 = Expression([
        Lambda(['z'], Variable('x')),
        Lambda(['x'], Expression([
            Lambda(['a'], Expression([
                Variable('a'), Variable('x')
            ])),
            Variable(7)
        ]))
    ])

    eval_expression_7 = copy.deepcopy(expression_7)
    eval_expression_7.applicative_evaluation()
    assert eval_expression_7.representation() == 'x'

    assert expression_7.representation() == '((lambda z. x) (lambda x. ((lambda a. (a x)) 7)))'
    assert expression_7.applicative_beta_reduction()
    assert expression_7.representation() == '((lambda z. x) (lambda x. (7 x)))'
    assert expression_7.applicative_beta_reduction()
    assert expression_7.representation() == 'x'
    assert not expression_7.applicative_beta_reduction()

    expression_8 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Variable('y')),
            Variable('x')
        ])),
        Lambda(['z'], Expression([
            Lambda(['a'], Expression([
                Variable('a'), Variable('z')
            ])),
            Variable('z')
        ]))
    ])

    eval_expression_8 = copy.deepcopy(expression_8)
    eval_expression_8.applicative_evaluation()
    assert eval_expression_8.representation() == '(lambda z. (z z))'

    assert expression_8.representation() == '((lambda x. ((lambda y. y) x)) (lambda z. ((lambda a. (a z)) z)))'
    assert expression_8.applicative_beta_reduction()
    assert expression_8.representation() == '((lambda x. x) (lambda z. ((lambda a. (a z)) z)))'
    assert expression_8.applicative_beta_reduction()
    assert expression_8.representation() == '((lambda x. x) (lambda z. (z z)))'
    assert expression_8.applicative_beta_reduction()
    assert expression_8.representation() == '(lambda z. (z z))'
    assert not expression_8.applicative_beta_reduction()

    expression_9 = Expression([
        Lambda(['a', 'b'], Expression([
            Variable('a'), Variable('a'), Variable('b'), Variable('b')
        ])),
        Expression([
            Lambda(['x', 'a'], Variable('a')),
            Lambda(['x'], Expression([
                Variable('x'),
                Variable('x')
            ])),
            Variable('a')
        ]),
        Variable('d')
    ])

    eval_expression_9 = copy.deepcopy(expression_9)
    eval_expression_9.applicative_evaluation()
    assert eval_expression_9.representation() == '(a a d d)'

    assert expression_9.representation() == '((lambda a b. (a a b b)) ((lambda x a. a) (lambda x. (x x)) a) d)'
    expression_9.applicative_beta_reduction()
    assert expression_9.representation() == '((lambda a b. (a a b b)) ((lambda a. a) a) d)'
    expression_9.applicative_beta_reduction()
    assert expression_9.representation() == '((lambda a b. (a a b b)) a d)'
    expression_9.applicative_beta_reduction()
    assert expression_9.representation() == '((lambda b. (a a b b)) d)'
    expression_9.applicative_beta_reduction()
    assert expression_9.representation() == '(a a d d)'

    expression_10 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Variable('y'),
                Lambda(['x'], Variable('x')),
                Variable('x')
            ]))
        ])),
        Variable('x')
    ])

    assert expression_10.representation() == '((lambda x. (lambda y. (y (lambda x. x) x))) x)'
    expression_10.applicative_beta_reduction()
    assert expression_10.representation() == "(lambda y. (y (lambda x'. x') x))"
    assert not expression_10.applicative_beta_reduction()

    print(GREEN + 'Passed' + END)
