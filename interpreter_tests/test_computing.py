import copy

from interpreter.expression import Expression
from interpreter.variable import Variable
from interpreter.lambda_element import Lambda
from interpreter_tests.colors import GREEN, END


def test_computing():
    print('Test computing: ', end='')

    true = Lambda(['t', 'f'], Variable('t'))
    false = Lambda(['t', 'f'], Variable('f'))

    # (λx. x (λt. λf. f) (λx. λt. λf. x f t) (λt. λf. f))
    zero = Lambda(['x'], Expression([
        Variable('x'),
        Lambda(['t', 'f'], Variable('f')),
        Lambda(['x', 't', 'f'], Expression([
            Variable('x'), Variable('f'), Variable('t')
        ])),
        Lambda(['t', 'f'], Variable('f')),
    ]))

    zero_true = Expression([
        copy.deepcopy(zero), copy.deepcopy(true)
    ])

    zero_false = Expression([
        copy.deepcopy(zero), copy.deepcopy(false)
    ])

    # (λs. (λz. (s z)))
    one = Lambda(['s'], Lambda(['z'], Expression([
        Variable('s'), Variable('z')
    ])))

    # (λx. λy. λz. x (y z))
    multiply = Lambda(['x', 'y', 'z'], Expression([
        Variable('x'), Expression([
            Variable('y'), Variable('z')
        ])
    ]))

    # (λx.λs.λz.x(λf.λg.g(f s)) (λg.z)(λm.m))
    subtract_one = Lambda(['x', 's', 'z'], Expression([
        Variable('x'),
        Lambda(['f', 'g'], Expression([
            Variable('g'), Expression([
                Variable('f'), Variable('s')
            ])
        ])),
        Lambda(['g'], Variable('z')),
        Lambda(['m'], Variable('m'))
    ]))

    # = (λs. (λz. (s (s (s (s (s z)))))
    five = Lambda(['s', 'z'], Expression([
        Variable('s'), Expression([
            Variable('s'), Expression([
                Variable('s'), Expression([
                    Variable('s'), Expression([
                        Variable('s'), Variable('z')
                    ])
                ])
            ])
        ])
    ]))

    three = Lambda(['s', 'z'], Expression([
        Variable('s'), Expression([
            Variable('s'), Expression([
                Variable('s'), Variable('z')
            ])
        ])
    ]))

    one_times_five = Expression([
        copy.deepcopy(multiply), copy.deepcopy(one), copy.deepcopy(five)
    ])

    one_minus_one = Expression([
        copy.deepcopy(subtract_one), copy.deepcopy(one)
    ])

    zero_true.normal_evaluation()
    assert zero_true.representation() == '(lambda f. f)'

    zero_false.normal_evaluation()
    assert zero_false.representation() == '(lambda t f. t)'

    one_minus_one.normal_evaluation()
    assert one_minus_one.representation() == '(lambda s z. z)'

    one_times_five.applicative_evaluation()
    assert one_times_five.representation() == "(lambda z. (lambda z'. (z (z (z (z (z z')))))))"
    print(GREEN + 'Passed' + END)

