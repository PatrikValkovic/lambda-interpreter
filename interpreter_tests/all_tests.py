from interpreter_tests.test_normal_evaluation import test_beta_reduction
from interpreter_tests.test_expression import test_expression_variable_detection, test_expression_representation
from interpreter_tests.test_applicative_evaluation import test_applicative_evaluation
from interpreter_tests.test_computing import test_computing
from interpreter_tests.test_minimal import test_minimal_expression
from interpreter_tests.test_alpha_reduction import test_alpha_reduction


def run():
    test_expression_representation()
    test_expression_variable_detection()
    test_beta_reduction()
    test_applicative_evaluation()
    test_computing()
    test_minimal_expression()
    test_alpha_reduction()

if __name__ == '__main__':
    run()
