from interpreter.lambda_element import Lambda
from interpreter.variable import Variable
from interpreter.expression import Expression
from interpreter_tests.colors import GREEN, END


def test_minimal_expression():
    print('Test minimal expression: ', end='')

    expression_0 = Expression([
        Lambda(['x'], Expression([Variable('x'), Variable('y')])),
        Variable(8)
    ])

    expression_1 = Expression([
        Expression([
            Lambda(['f'], Variable('f')),
            Lambda(['y'], Variable('y'))
        ]),
        Lambda(['z'], Variable('z')),
        Lambda(['x'], Variable('x'))
    ])

    expression_2 = Expression([
        Lambda(['x'], Expression([
            Lambda(['y'], Expression([
                Lambda(['z'], Expression([
                    Variable('x'), Variable('y')
                ])),
                Variable('z')
            ])),
            Variable('f')
        ]))
    ])

    expression_3 = Expression([
        Lambda(['x'], Expression([
            Variable('x')
        ])),
        Lambda(['y'], Expression([
            Variable('y')
        ])),
    ])

    assert not expression_0.is_minimal()
    expression_0.normal_evaluation()
    assert expression_0.is_minimal()

    assert not expression_1.is_minimal()
    expression_1.normal_evaluation()
    assert expression_1.is_minimal()

    assert not expression_2.is_minimal()
    expression_2.normal_evaluation()
    assert expression_2.is_minimal()

    assert not expression_3.is_minimal()
    expression_3.normal_evaluation()
    assert expression_3.is_minimal()

    print(GREEN + 'Passed' + END)

